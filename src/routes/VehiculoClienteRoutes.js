const VehiculoCliente = require('../models/VehiculoCliente');

module.exports = function(app) {
    app.get('/vehiculo-cliente/:idCliente', (req,res) => {
        VehiculoCliente.getById(req.params.idCliente).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results[0]);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/vehiculo-cliente', (req, res) => {
        const vehiculoClienteData = {
            idMarcaVehiculo: req.body.idMarcaVehiculo,
            idLineaVehiculo: req.body.idLineaVehiculo,
            idTipoVehiculo: req.body.idTipoVehiculo,
            idUsoVehiculo: req.body.idUsoVehiculo,
            idTipoCombustible: req.body.idTipoCombustible,
            idTransmision: req.body.idTransmision,
            color: req.body.color,
            cantPuertas: req.body.cantPuertas,
            modelo: req.body.modelo,
            placa: req.body.placa,
            idTamanioMotor: req.body.idTamanioMotor,
            idCliente: req.body.idCliente
        }    
        VehiculoCliente.create(vehiculoClienteData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Vehiculo cliente agregado con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
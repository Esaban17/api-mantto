const Cliente = require('../models/Cliente');
const bcrypt = require('bcrypt');

module.exports = function(app) {
    app.get('/cliente', (req,res) => {
        Cliente.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/cliente', (req, res) => {
        let passwordEncrypt;
        bcrypt.hash(req.body.password, 10).then(hash => {
            passwordEncrypt = hash;
            const clienteData = {
                idCliente: null,
                email: req.body.email,
                password: passwordEncrypt,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                telefono: req.body.telefono,
                identificacion: req.body.identificacion,
                direccion: req.body.direccion,
                pais: req.body.pais
            }
            Cliente.create(clienteData).then(results => {
                if (typeof results !== 'undefined' && results.length > 0) {
                    res.status(200).json(results);
                } else {
                    res.status(200).json({message: "Cliente creado con éxito"});
                }
            },(err) => {
                console.log(err)
                res.status(500).json(err);
            });
        });
    });
}
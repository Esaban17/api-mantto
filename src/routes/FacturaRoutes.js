const Factura = require('../models/Factura');

module.exports = function(app) {
    app.get('/factura', (req,res) => {
        Factura.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/factura', (req, res) => {
        const facturaData = {
            idSolicitudServicio: req.body.idSolicitudServicio,
            idCliente: req.body.idCliente,
            idTipoPago: req.body.idTipoPago,
            idMoneda: req.body.idMoneda,
            idPromocion: req.body.idPromocion,
            descripcion: req.body.descripcion,
            cantidad: req.body.cantidad,
            hora: req.body.hora,
            fecha: req.body.fecha,
            monto: req.body.monto
        }
        Factura.create(facturaData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Factura creada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
const Transmision = require('../models/Transmision');

module.exports = function(app) {
    app.get('/transmision', (req,res) => {
        Transmision.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });
}
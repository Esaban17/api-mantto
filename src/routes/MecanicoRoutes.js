const Mecanico = require('../models/Mecanico');
const bcrypt = require('bcrypt');

module.exports = function (app) {
    app.get('/mecanico', (req, res) => {
        Mecanico.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({ message: 'Not Content' });
            }
        }, (err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.get('/mecanico/:idMecanico', (req, res) => {
        Mecanico.getById(req.params.idMecanico).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({ message: 'Not Content' });
            }
        }, (err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/mecanico', (req, res) => {
        let passwordEncrypt;
        bcrypt.hash(req.body.password, 10).then(hash => {
            passwordEncrypt = hash;
            const mecanicoData = {
                idMecanico: null,
                email: req.body.email,
                password: passwordEncrypt,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                telefono: req.body.telefono,
                identificacion: req.body.identificacion,
                direccion: req.body.direccion,
                marcaVehiculo: req.body.marcaVehiculo,
                placa: req.body.placa,
                idTipoTarjeta: req.body.idTipoTarjeta,
                numTarjeta: req.body.numTarjeta,
                fechaVencimiento: req.body.fechaVencimiento,
                codigoSeguridad: req.body.codigoSeguridad,
                pais: req.body.pais,
                idBanco: req.body.idBanco
            }
            Mecanico.create(mecanicoData).then(results => {
                if (typeof results !== 'undefined' && results.length > 0) {
                    res.status(200).json(results);
                } else {
                    res.status(200).json({ message: "Mecanico creado con éxito" });
                }
            }, (err) => {
                console.log(err)
                res.status(500).json(err);
            });
        });
    });
}
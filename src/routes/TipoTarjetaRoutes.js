const TipoTarjeta = require('../models/TipoTarjeta');

module.exports = function(app) {
    app.get('/tipo-tarjeta', (req,res) => {
        TipoTarjeta.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/tipo-tarjeta', (req, res) => {
        const tipoTarjetaData = {
            idTipoTarjeta: null,
            tipoTarjeta: req.body.tipoTarjeta
        }
        TipoTarjeta.create(tipoTarjetaData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json(results);
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
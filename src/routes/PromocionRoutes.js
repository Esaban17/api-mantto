const Promocion = require('../models/Promocion');

module.exports = function(app) {
    app.get('/promocion', (req,res) => {
        Promocion.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/promocion', (req, res) => {
        const promocionData = {
            idTipoPromocion: req.body.idTipoPromocion,
            promocion: req.body.promocion,
            descuento: req.body.descuento,
            fechaCreacion: req.body.fechaCreacion,
            fechaInicio: req.body.fechaInicio,
            fechaFin: req.body.fechaFin
        }
        Promocion.create(promocionData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Promocion registrada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
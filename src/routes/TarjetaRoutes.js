const Tarjeta = require('../models/Tarjeta');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = function(app) {
    app.post('/tarjeta', (req, res) => {
        let encriptada;
        bcrypt.hash(req.body.numTarjeta, saltRounds).then(hash => {
            encriptada = hash;
            const tarjetaData = {
                idTarjeta: null,
                idTipoTarjeta: req.body.idTipoTarjeta,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                numTarjeta: encriptada,
                fechaVencimiento: req.body.fechaVencimiento,
                codigoSeguridad: req.body.codigoSeguridad,
                idPais: req.body.idPais,
                idBanco: req.body.idBanco,
                idMoneda: req.body.idMoneda
            }
            Tarjeta.create(tarjetaData).then(results => {
                if (typeof results !== 'undefined' && results.length > 0) {
                    res.status(200).json(results);
                } else {
                    res.status(200).json({message: "Tarjeta creada con éxito"});
                }
            },(err) => {
                console.log(err)
                res.status(500).json(err);
            });
        },(err) => {
            res.status(500).json({message: "Error No. de tarjeta necesario"});
        });
    });
}
const TipoPromocion = require('../models/TipoPromocion');

module.exports = function(app) {
    app.get('/tipo-promocion', (req,res) => {
        TipoPromocion.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        })
    });

    app.post('/tipo-promocion', (req, res) => {
        const tipoPromocionData = {
            tipoPromocion: req.body.tipoPromocion
        }
        TipoPromocion.create(tipoPromocionData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Tipo promocion registrada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
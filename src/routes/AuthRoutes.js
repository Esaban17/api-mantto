var jwt = require('jsonwebtoken');
var Usuario = require('../models/Usuario');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = function (app) {
    app.post('/login', function (req, res) {
        var usuarioData = {
            idUsuario: null,
            email: req.body.email
        }
        Usuario.login(usuarioData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                bcrypt.compare(req.body.password, results[0].password).then(function (sonIguales) {
                    if (sonIguales) {
                        var temp = {
                            idUsuario: results[0].idUsuario,
                            idPersona: results[0].idPersona,
                            email: results[0].email,
                            password: results[0].password,
                            tipoUsuario: results[0].tipoUsuario
                        }

                        var token = jwt.sign(temp, 'mantto', { expiresIn: "365 days" });
                        var resultado = {
                            idUsuario: results[0].idUsuario,
                            token: token
                        }
                        res.json(resultado);
                    }else{
                        res.json({mensaje: "Password Incorrecto"})
                    }
                });
            } else {
                res.json({
                    mensaje: "Email Incorrecto"
                });
            }
        })
    });
}
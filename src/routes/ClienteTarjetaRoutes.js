const ClienteTarjeta = require('../models/ClienteTarjeta');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = function(app) {
    app.post('/cliente-tarjeta', (req, res) => {
        let encriptada;
        bcrypt.hash(req.body.numTarjeta, saltRounds).then(hash => {
            encriptada = hash;
            const tarjetaData = {
                idTarjeta: null,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                numTarjeta: encriptada,
                idTipoTarjeta: req.body.idTipoTarjeta,
                fechaVencimiento: req.body.fechaVencimiento,
                codigoSeguridad: req.body.codigoSeguridad,
                idCliente: req.body.idCliente,
                idBanco: req.body.idBanco,
                pais: req.body.pais
            }
            ClienteTarjeta.create(tarjetaData).then(results => {
                if (typeof results !== 'undefined' && results.length > 0) {
                    res.status(200).json(results);
                } else {
                    res.status(200).json({message: "Tarjeta cliente agregada con éxito"});
                }
            },(err) => {
                console.log(err)
                res.status(500).json(err);
            });
        },(err) => {
            res.status(500).json({message: "Error No. de tarjeta necesario"});
        });
    });
}
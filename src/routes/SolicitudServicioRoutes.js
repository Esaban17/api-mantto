const SolicitudServicio = require('../models/SolicitudServicio');

module.exports = function(app) {
    app.get('/solicitud-servicio', (req,res) => {
        SolicitudServicio.getAll().then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(204).json({message: 'Not Content'});
            }
        },(err) => {
            res.status(500).json(err);
        })
    });

    app.put('/solicitud-servicio/aceptada/:idSolicitudServicio', (req, res) => {
        const solicitudServicioData = {
            idSolicitudServicio: req.params.idSolicitudServicio,
            idMecanico: req.body.idMecanico,
            horaLlegando: req.body.horaLlegando,
        }    
        SolicitudServicio.aceptada(solicitudServicioData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Solicitud de servicio aceptada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });

    app.put('/solicitud-servicio/atendida/:idSolicitudServicio', (req, res) => {
        const solicitudServicioData = {
            idSolicitudServicio: req.params.idSolicitudServicio,
            horaAtencion: req.body.horaAtencion,
        }    
        SolicitudServicio.atendida(solicitudServicioData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Solicitud de servicio atendida con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });

    app.put('/solicitud-servicio/finalizada/:idSolicitudServicio', (req, res) => {
        const solicitudServicioData = {
            idSolicitudServicio: req.params.idSolicitudServicio,
            horaFinalizacion: req.body.horaFinalizacion
        }    
        SolicitudServicio.finalizada(solicitudServicioData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Solicitud de servicio finalizada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });

    app.post('/solicitud-servicio', (req, res) => {
        const solicitudServicioData = {
            idServicio: req.body.idServicio,
            idCliente: req.body.idCliente,
            idTipoPago: req.body.idTipoPago,
            horaSolicitud: req.body.horaSolicitud,
            latitud: req.body.latitud,
            longitud: req.body.longitud
        }    
        SolicitudServicio.create(solicitudServicioData).then(results => {
            if (typeof results !== 'undefined' && results.length > 0) {
                res.status(200).json(results);
            } else {
                res.status(200).json({message: "Solicitud de servicio creada con éxito"});
            }
        },(err) => {
            console.log(err)
            res.status(500).json(err);
        });
    });
}
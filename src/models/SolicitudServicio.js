var pool = require('../db/conexion');
let solicitudServicioModel = {};

solicitudServicioModel.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                connection.query('SELECT * FROM solicitud_servicio WHERE estado = "Pendiente"',
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    })
}

solicitudServicioModel.create = (solicitudServicioData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_InsertarSolicitudServicio(?,?,?,?,?,?)";
                connection.query(sql, [solicitudServicioData.idServicio, solicitudServicioData.idCliente, solicitudServicioData.idTipoPago, solicitudServicioData.horaSolicitud,
                solicitudServicioData.latitud, solicitudServicioData.longitud],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

solicitudServicioModel.aceptada = (solicitudServicioData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_SolicitudServicioAceptada(?,?,?)";
                connection.query(sql, [solicitudServicioData.idSolicitudServicio, solicitudServicioData.idMecanico, solicitudServicioData.horaLlegando],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

solicitudServicioModel.atendida = (solicitudServicioData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_SolicitudServicioAtendida(?,?)";
                connection.query(sql, [solicitudServicioData.idSolicitudServicio, solicitudServicioData.horaAtencion],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

solicitudServicioModel.finalizada = (solicitudServicioData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_SolicitudServicioFinalizada(?,?)";
                connection.query(sql, [solicitudServicioData.idSolicitudServicio, solicitudServicioData.horaFinalizacion],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

module.exports = solicitudServicioModel
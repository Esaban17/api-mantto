var pool = require('../db/conexion');
let clienteTarjetaModel = {};

clienteTarjetaModel.create = (clienteTarjetaData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_InsertarClienteTarjeta(?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [clienteTarjetaData.nombres,clienteTarjetaData.apellidos,clienteTarjetaData.numTarjeta,clienteTarjetaData.idTipoTarjeta,
                clienteTarjetaData.fechaVencimiento,clienteTarjetaData.codigoSeguridad,clienteTarjetaData.idCliente,clienteTarjetaData.idBanco,clienteTarjetaData.pais],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

module.exports = clienteTarjetaModel
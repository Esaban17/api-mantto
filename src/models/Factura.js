var pool = require('../db/conexion');
let facturaModel = {};

facturaModel.create = (facturaData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_InsertarFactura(?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [facturaData.idSolicitudServicio, facturaData.idCliente, facturaData.idTipoPago, facturaData.idMoneda,
                facturaData.idPromocion, facturaData.descripcion, facturaData.cantidad, facturaData.hora, facturaData.fecha, facturaData.monto],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    });
}


module.exports = facturaModel
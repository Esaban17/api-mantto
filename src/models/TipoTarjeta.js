var pool = require('../db/conexion');
let tipoTarjetaModel = {};

tipoTarjetaModel.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                connection.query('SELECT * FROM tipo_tarjeta',
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    })
}

tipoTarjetaModel.create = (tipoTarjetaData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if(err){
                console.log(err);
            }else{
                const sql = "CALL SP_InsertarTipoTarjeta(?)";
                connection.query(sql, [tipoTarjetaData.tipoTarjeta],
                (err, results) => {
                    if(err) {
                        return reject(err);
                    }else{
                        return resolve({message: "Tipo tarjeta registrada con éxito"});
                    }
                });
            }
            connection.release();
        })
    })
}

module.exports = tipoTarjetaModel
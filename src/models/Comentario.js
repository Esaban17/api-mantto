var pool = require('../db/conexion');
let comentarioModel = {};

comentarioModel.getById = (idCliente) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = `CALL SP_ObtenerLineaVehiculo(${connection.escape(idMarcaVehiculo)})`;
                connection.query(sql, (err, results) => {
                    if (err) {
                        return reject(err);
                    } else {
                        return resolve(results);
                    }
                });
            }
            connection.release();
        });
    });
}

comentarioModel.create = (mecanicoData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_InsertarMecanico(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [mecanicoData.email, mecanicoData.password, mecanicoData.nombres, mecanicoData.apellidos, mecanicoData.telefono, mecanicoData.identificacion,
                mecanicoData.direccion, mecanicoData.marcaVehiculo, mecanicoData.placa, mecanicoData.tipoTarjeta, mecanicoData.numTarjeta, mecanicoData.fechaVencimiento, mecanicoData.pais,
                mecanicoData.idBanco],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        })
    })
}

module.exports = comentarioModel
var pool = require('../db/conexion');
let tipoPromocion = {};

tipoPromocion.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                connection.query('SELECT * FROM tipo_promocion',
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    })
}

tipoPromocion.create = (tipoPromocionData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if(err){
                console.log(err);
            }else{
                const sql = "CALL SP_InsertarTipoPromocion(?)";
                connection.query(sql, [tipoPromocionData.tipoPromocion],
                (err, results) => {
                    if(err) {
                        return reject(err);
                    }else{
                        return resolve(results);
                    }
                });
            }
            connection.release();
        })
    })
}

module.exports = tipoPromocion
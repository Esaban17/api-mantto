var pool = require('../db/conexion');
let promocionModel = {};

promocionModel.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                connection.query('SELECT * FROM promocion',
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    })
}

promocionModel.create = (promocionData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if(err){
                console.log(err);
            }else{
                const sql = "CALL SP_InsertarPromocion(?,?,?,?,?,?)";
                connection.query(sql, [promocionData.idTipoPromocion,promocionData.promocion,promocionData.descuento,promocionData.fechaCreacion,promocionData.fechaInicio,
                promocionData.fechaFin],
                (err, results) => {
                    if(err) {
                        return reject(err);
                    }else{
                        return resolve(results);
                    }
                });
            }
            connection.release();
        })
    })
}

module.exports = promocionModel
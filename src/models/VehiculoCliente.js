var pool = require('../db/conexion');
let vehiculoClienteModel = {};

vehiculoClienteModel.getById = (idCliente) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = `CALL SP_ObtenerVehiculosCliente(${connection.escape(idCliente)})`;
                connection.query(sql, (err, results) => {
                    if (err) {
                        return reject(err);
                    } else {
                        return resolve(results);
                    }
                });
            }
            connection.release();
        });
    });
}

vehiculoClienteModel.create = (vehiculoClienteData) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log(err);
            } else {
                const sql = "CALL SP_InsertarVehiculoCliente(?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [vehiculoClienteData.idMarcaVehiculo, vehiculoClienteData.idLineaVehiculo, vehiculoClienteData.idTipoVehiculo, vehiculoClienteData.idUsoVehiculo,
                vehiculoClienteData.idTipoCombustible, vehiculoClienteData.idTransmision, vehiculoClienteData.color, vehiculoClienteData.cantPuertas, vehiculoClienteData.modelo,
                vehiculoClienteData.placa, vehiculoClienteData.idTamanioMotor, vehiculoClienteData.idCliente],
                    (err, results) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve(results);
                        }
                    });
            }
            connection.release();
        });
    });
}


module.exports = vehiculoClienteModel